#!/usr/bin/env bash
#
# (C) Daniel Pöllmann, 2020
#
NEWLINE=$'\n'

# Read argument
if [ $# -eq 0 ]
  then
    echo "No arguments supplied. Run with 'whl.sh myProgram.while'"
    exit 1
fi
FILE=$1

# Create temporary file
if [ -f "${FILE}" ]; then
    echo "Creating temporary file at /tmp/program.c"
    cp "${FILE}" /tmp/program.c
else
    echo "${FILE} does not exist"
    exit 1
fi

# C Template Variables
read -r -d '' CHEADER << EOM
#include <stdio.h>
#include <stdlib.h>
void stop(int result) {
    printf("x0=%u\n", result);
    exit(0);
}
int minus(int a, int b) {
  return ((a-b) < 0) ? 0 : (a-b);
}
int plus(int a, int b) {
  if((a + b) < a) {
    printf("Integer overflow occured.");
    exit(1);
  } else {
    return a + b;
  }
}
int main() {
EOM

# Remove comments
sed -i '/^\s*#.*/d' /tmp/program.c
sed -i 's/#.*//g' /tmp/program.c

# Replace WHILE x1 =/= ... -> while (x1 != 0) {
sed -i 's/WHILE\s*\(x[0-9]*\).*DO/while(\1 != 0) {/g' /tmp/program.c

# Replace minus Assignments xn := xm - c -> xn = minus(xm, c)
sed -i 's/\(x[0-9]*\)\s*:=\s*\(x[0-9]*\)\s*-\s*\([0-9]*\)/\1=minus(\2,\3);/g' /tmp/program.c

# Replace plus Assignments xn := xm + c -> xn = plus(xm, c)
sed -i 's/\(x[0-9]*\)\s*:=\s*\(x[0-9]*\)\s*+\s*\([0-9]*\)/\1=plus(\2,\3);/g' /tmp/program.c

# Replace Assignments xn := xm op c -> xn = xm op c;
#sed -i 's/\(x[0-9]*\)\s*:=\s*\(x[0-9]*.*\)/\1=\2\;/g' /tmp/program.c

# Replace IF (xn = c) THEN GOTO Mn -> if (xn == 0) {goto Mn;}
sed -i 's/IF\s*\(x[0-9]*\)\s*=\s*\([0-9]*\)\s*THEN\s*GOTO\s*\(M[0-9]*\)/if (\1 == \2) {goto \3;}/g' /tmp/program.c

# Replace GOTO Mn -> goto Mn;
sed -i 's/GOTO\s*\(M[0-9]*\)/goto \1\;/g' /tmp/program.c

# Add empty statement after Label: Mn: -> Mn: ;
sed -i 's/\(M[0-9]*:\)/\1 \;/g' /tmp/program.c

# Replace LOOP xn DO ->
# {
# int counter = xn;
# for (int i <= counter) {
# }
sed -i 's/LOOP\s*\(x[0-9]*\)\s*DO/int counter = \1\;\n for (int i=0; i<counter;i++) { /g' /tmp/program.c

# Replace END -> }
sed -i 's/END/}/g' /tmp/program.c

# Replace HALT -> stop()
sed -i 's/HALT/stop(x0)\;/g' /tmp/program.c

# Replace two consecutive "counter" with cntrN where N is some int
i=0
while grep "counter" /tmp/program.c > /dev/null;
do
    sed -i "0,/counter/s//cntr${i}/" /tmp/program.c
    sed -i "0,/counter/s//cntr${i}/" /tmp/program.c
    ((i=i+1))
done

# Find all variables of form xn
VARS=$(cat /tmp/program.c | grep -o 'x[0-9]*')
VARS+=('x0')
VARSTODECLARE=$(printf "%s\n" "${VARS[@]}" | tr ' ' '\n' | sort -n | uniq)
V=($VARSTODECLARE)

# Create C Variable declaration (init with 0)
VARDECL=""
for i in "${V[@]}"
do
   VARDECL="${VARDECL}${NEWLINE}  unsigned int ${i}=0;"
done

# Assemble parts of program
echo "${CHEADER}${NEWLINE}${VARDECL}${NEWLINE}$(cat /tmp/program.c);${NEWLINE}stop(x0);${NEWLINE}}" > /tmp/program.c

mv /tmp/program.c "${FILE}.c"

# Output program
cat "${FILE}.c"
